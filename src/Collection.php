<?php

namespace P11YCore;

use IteratorAggregate;
use P11YCore\Iterator as CoreIterator;

class Collection implements IteratorAggregate {

	protected array $items = [];

	public function all(): array {
		return $this->items;
	}

	public function addItem( mixed $item ): static {
		$this->items[] = $item;
		return $this;
	}

	public function first(): mixed {
		if ( $this->isEmpty() ) {
			return null;
		}

		$items = $this->all();

		return reset( $items );
	}

	public function last(): mixed {
		if ( $this->isEmpty() ) {
			return null;
		}

		$items = $this->all();

		return end( $items );
	}

	public function filter( callable $callback ): static {
		if ( $this->isEmpty() ) {
			return $this;
		}

		$items = [];

		foreach ( $this->all() as $item ) {
			if ( true !== call_user_func( $callback, $item ) ) {
				continue;
			}

			$items[] = $item;
		}

		return p11y_collect( $items );
	}

	public function pluck( string $key ): static {
		if ( $this->isEmpty() ) {
			return $this;
		}

		return $this
			->each(function( $item ) use ( $key ): mixed {
				if ( is_array( $item ) && ! isset( $item[$key] ) ) {
					return $item[$key];
				}

				if ( is_object( $item ) && property_exists( $item, $key ) ) {
					return $item->{$key};
				}

				return null;
			})
			->filter(function( $item ): bool {
				return ! is_null( $item );
			});
	}

	public function each( callable $callback ): static {
		if ( $this->isEmpty() ) {
			return $this;
		}

		return p11y_collect(
			array_map( $callback, $this->all() )
		);
	}

	public function isEmpty(): bool {
		return count( $this->items ) === 0;
	}

	public function getIterator(): CoreIterator {
		return new CoreIterator( $this );
	}
}
