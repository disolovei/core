<?php

namespace P11YCore;

use Iterator as BaseIterator;

class Iterator implements BaseIterator {

	protected int $position = 0;

	public function __construct( 
        protected Collection $collection 
    ) {}

	public function current(): mixed {
		return $this->collection->all()[$this->position];
	}

	public function next(): void {
		$this->position += 1;
	}

	public function key(): int {
		return $this->position;
	}

	public function valid(): bool {
		return isset( $this->collection->all()[$this->position] );
	}

	public function rewind(): void {
		$this->position = 0;
	}
}
