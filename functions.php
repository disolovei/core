<?php

use P11YCore\Collection;

function p11y_is_collection( mixed $target ): bool {
	return $target instanceof Collection;
}

function p11y_collect( mixed $origin = null ): Collection {
	if ( p11y_is_collection( $origin ) ) {
		return $origin;
	}

	$collection = new Collection();

	if ( is_scalar( $origin ) || is_null( $origin ) || is_object( $origin ) ) {
		$collection->addItem( $origin );
		return $collection;
	}

	if ( is_array( $origin ) ) {
		foreach ( $origin as $item ) {
			$collection->addItem( $item );
		}
	}

	return $collection;
}
